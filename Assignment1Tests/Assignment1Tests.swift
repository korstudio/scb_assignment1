//
//  Assignment1Tests.swift
//  Assignment1Tests
//
//  Created by korstudio on 5/25/17.
//  Copyright © 2017 Methas Tariya. All rights reserved.
//

import XCTest
@testable import Assignment1

class Assignment1Tests: XCTestCase {
    
    let ballSorter:BallSorter = BallSorter()
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_calculate_inputN_more_than_50() {
        let input = "51 2\n1 2\n2 3\n3 4\n4 1"
        let expected = "ERROR"
        
        let result = ballSorter.calculate(input: input)
        
        XCTAssertEqual(result, expected)
    }
    
    func test_calculate_inputN_less_than_1() {
        let input = "0 2\n1 2\n2 3\n3 4\n4 1"
        let expected = "ERROR"
        
        let result = ballSorter.calculate(input: input)
        
        XCTAssertEqual(result, expected)
    }
    
    func test_calculate_inputK_more_than_1000000000() {
        let input = "4 1000000001\n1 2\n2 3\n3 4\n4 1"
        let expected = "ERROR"
        
        let result = ballSorter.calculate(input: input)
        
        XCTAssertEqual(result, expected)
    }
    
    func test_calculate_inputK_less_than_1() {
        let input = "4 0\n1 2\n2 3\n3 4\n4 1"
        let expected = "ERROR"
        
        let result = ballSorter.calculate(input: input)
        
        XCTAssertEqual(result, expected)
    }
    
    func test_calculate_inputN_not_matched_with_sets_count() {
        let input = "5 2\n1 2\n2 3\n3 4\n4 1"
        let expected = "ERROR"
        
        let result = ballSorter.calculate(input: input)
        
        XCTAssertEqual(result, expected)
    }
    
    func test_calculate_input1_correct() {
        let input = "4 2\n1 2\n2 3\n3 4\n4 1"
        let expected = "1 4 2 3 5 6 7 8\n"
        
        let result = ballSorter.calculate(input: input)
        
        XCTAssertEqual(result, expected)
    }
    
    func test_calculate_input2_correct() {
        let input = "16 1000000000\n1 3\n6 8\n3 5\n2 6\n3 7\n3 4\n4 7\n2 4\n1 3\n2 7\n2 7\n2 4\n6 7\n1 7\n3 4\n1 6"
        let expected = "1 8 3 4 5 2 7 6\n"
        
        let result = ballSorter.calculate(input: input)
        
        XCTAssertEqual(result, expected)
    }
    
    func test_calculate_performance() {
        self.measure {
            let input = "16 1000000000\n1 3\n6 8\n3 5\n2 6\n3 7\n3 4\n4 7\n2 4\n1 3\n2 7\n2 7\n2 4\n6 7\n1 7\n3 4\n1 6"
            let expected = "1 8 3 4 5 2 7 6\n"
            let result = self.ballSorter.calculate(input: input)
            XCTAssertEqual(result, expected)
        }
    }
    
}
