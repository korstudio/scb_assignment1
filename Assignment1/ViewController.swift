//
//  ViewController.swift
//  Assignment1
//
//  Created by korstudio on 5/25/17.
//  Copyright © 2017 Methas Tariya. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputText: UITextView!
    @IBOutlet weak var resultText: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    private let ballSorter = BallSorter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        indicator.hidesWhenStopped = true
        
        calculateAndUpdate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    private func calculateAndUpdate() {
        indicator.startAnimating()
        
        let input = inputText.text
        
        DispatchQueue.global(qos: .background).async { 
            let result = self.ballSorter.calculate(input: input!)
            DispatchQueue.main.async {
                self.updateResultText(text: result)
                self.indicator.stopAnimating()
            }
        }
        
    }
    
    private func updateResultText(text txt:String) {
        resultText.text = txt;
    }
    
    // MARK: button listener
    @IBAction func onClickGo(_ sender: Any) {
        calculateAndUpdate()
    }
}

