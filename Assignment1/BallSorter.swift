//
//  BallSorter.swift
//  Assignment1
//
//  Created by korstudio on 5/26/17.
//  Copyright © 2017 Methas Tariya. All rights reserved.
//

import Foundation

class BallSorter: NSObject {
    
    public func calculate(input str:String) -> String {
        var ballPool = ["1", "2", "3", "4", "5", "6", "7", "8"];
        var result = ""
        
        let separatedInput = str.components(separatedBy: CharacterSet(charactersIn: " \n"))
        let len = separatedInput.count
        var inputArr = [(Int,Int)]()
        var error = false
        
        var numOfSet = 0
        var numOfRepeat = 0
        
        if(len%2 > 0) {
            error = true
        }
        
        print(separatedInput)
        
        if(!error) {
            for index in stride(from: 0, to: len, by: 2) {
                let nextIndex = index + 1
                let indexInt:Int? = Int(separatedInput[index])
                let nextIndexInt:Int? = Int(separatedInput[nextIndex])
                if indexInt == nil || nextIndexInt == nil {
                    error = true
                } else {
                    inputArr.append((indexInt!, nextIndexInt!))
                }
            }
        }
        
        if(!error) {
            numOfSet = inputArr[0].0
            numOfRepeat = inputArr[0].1
            if(numOfSet > 50 || numOfSet < 1) {
                error = true
            }
            if(numOfRepeat < 1 || numOfRepeat > 1000000000) {
                error = true
            }
            if(inputArr.count - 1 != numOfSet) {
                error = true
            }
        }
        
        if(!error) {
            var sampleArr = [String]()
            
            for _ in stride(from: 0, to: numOfSet-1, by: 1) {
                for si in stride(from: 0, to: numOfSet, by: 1) {
                    let set = inputArr[si+1]
                    let setIndex = (set.0 - 1, set.1 - 1)
                    
                    let temp = ballPool[setIndex.1]
                    ballPool[setIndex.1] = ballPool[setIndex.0]
                    ballPool[setIndex.0] = temp
                }
                sampleArr.append(ballPool.joined(separator: " "))
            }
            
            var targetIndex = (numOfRepeat % (numOfSet-1)) - 1
            if targetIndex < 0 {
                targetIndex = sampleArr.count - 1
            }
            
            print("samples \(sampleArr)")
            print("target sampleArr[\(targetIndex)] = \(sampleArr[targetIndex])")
            result = sampleArr[targetIndex];
            
            // Old way -  too slow
//            for ri in stride(from: 0, to: numOfRepeat, by: 1) {
//                for si in stride(from: 0, to: numOfSet, by: 1) {
//                    let set = inputArr[si+1]
//                    let setIndex = (set.0 - 1, set.1 - 1)
//                    
//                    let temp = ballPool[setIndex.1]
//                    ballPool[setIndex.1] = ballPool[setIndex.0]
//                    ballPool[setIndex.0] = temp
//                }
//            }
            
        }
        
        if(error) {
            return "ERROR"
        }
        
        return "\(result)\n"
    }
    
}
