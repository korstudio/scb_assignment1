Ball Sorter
==============

BallSorter.swift
------------

This class takes responsibility for interprete the input and operate the sorting based on input in each row.

First thing first, prepare the ball pool to be sorted

```
var ballPool = ["1", "2", "3", "4", "5", "6", "7", "8"];
var result = ""
```

Assume we have this set of input:

```
4 2
1 2
2 3
3 4
4 1
```

After get that input, the input is splitted by spaces and newlines

```
let separatedInput = str.components(separatedBy: CharacterSet(charactersIn: " \n"))
let len = separatedInput.count
```

The result of `separatedInput` will be:
`["4", "2", "1", "2", "2", "3", "3", "4", "4", "1"]`

Initialize the mandatory properties

```
var inputArr = [(Int,Int)]()
var error = false

var numOfSet = 0
var numOfRepeat = 0
```

Check the `separatedInput` first whether we have them in pairs or not. They must initially come in pairs.
If not, set `error` to `true` and wait to be returned at the end of the function.

```
if(len%2 > 0) {
    error = true
}
```

Loop through the `separatedInput` to convert each of them to Int and put in tuple pair by pair.
Of course, we have to check if we have an error or not.

```
if(!error) {
    for index in stride(from: 0, to: len, by: 2) {
        let nextIndex = index + 1
        let indexInt:Int? = Int(separatedInput[index])
        let nextIndexInt:Int? = Int(separatedInput[nextIndex])
        if indexInt == nil || nextIndexInt == nil {
            error = true
        } else {
            inputArr.append((indexInt!, nextIndexInt!))
        }
    }
}
```

Next, check the first pair of the `inputArr` which is the information about the input (namely N and K in the assignment doc).
They must meet our conditions before moving on to next step.

```
if(!error) {
    numOfSet = inputArr[0].0
    numOfRepeat = inputArr[0].1
    if(numOfSet > 50 || numOfSet < 1) {
        error = true
    }
    if(numOfRepeat < 1 || numOfRepeat > 1000000000) {
        error = true
    }
    if(inputArr.count - 1 != numOfSet) {
        error = true
    }
}
```

Notice that we have to check the input sets amount as well.

The next part is the heart of the program: calculate and sort the balls.
Again, we have to check whether there is an error first.

(Starting from this point, I will assume that the codes showing here are wrapped by `if(!error)`)

First in this section, my idea is to create the "samples" of the possible data. 
From what I have noticed, the data flows in concrete patterns. 
They will always have only N-1 samples then it loops to the first of the samples again.

```
var sampleArr = [String]()
            
for _ in stride(from: 0, to: numOfSet-1, by: 1) {
    for si in stride(from: 0, to: numOfSet, by: 1) {
        let set = inputArr[si+1] //We have to +1 because the first line is "information" line (N,K)
        let setIndex = (set.0 - 1, set.1 - 1) //We have to -1 from the set because the number input is 1-based number but array is 0-based number
        
        //swap items
        let temp = ballPool[setIndex.1]
        ballPool[setIndex.1] = ballPool[setIndex.0]
        ballPool[setIndex.0] = temp
    }

    //store the sample
    sampleArr.append(ballPool.joined(separator: " "))
}
```

_Note_: `stride` enabled us to use variable in for-loop since Swift 3 has only shorthand loop like: for i in 0..1 

Sample example from mentioned input:

`samples ["1 3 4 2 5 6 7 8", "1 4 2 3 5 6 7 8", "1 2 3 4 5 6 7 8"]`

(N sets have N-1 samples, so we have 4 sets then we have 3 samples)

After we get our possible samples, now we have to get the target index for picking up the sample we want.
By using `numOfRepeat % (numofSet-1)`, we will get the remainder in 1-based number so we have to, again, minus with 1.

```
var targetIndex = (numOfRepeat % (numOfSet-1)) - 1
if targetIndex < 0 {
    targetIndex = sampleArr.count - 1
}
```
Be aware of 0 remainder. If we found that remainder is 0, we will have to get the last one of the sample instead. Like a loop.

In this case, we have 2 as the repeation number (`numOfRepeat` or K), 3 samples, (2%3) - 1 is 1
So, we get our sample in `sampleArr[1]`

`target sampleArr[1] = 1 4 2 3 5 6 7 8`

And this is the correct answer

- - - - -

I further try using another test case from the assignment.

```
16 1000000000
1 3
6 8
3 5
2 6
3 7
3 4
4 7
2 4
1 3
2 7
2 7
2 4
6 7
1 7
3 4
1 6
```

The output is `1 8 3 4 5 2 7 6`
which is correct and take time less than 1 second.

`Test Case '-[Assignment1Tests.Assignment1Tests test_calculate_performance]' passed (0.266 seconds).`

(Test using XCTest's `measure` function)

- - - - -

ViewController.swift
---------

This controller handles the view (outlets) and actions. Also make a call to BallSorter.calculate()

I also put the calculate() call into background thread in case of the calculation is longer than expected so we can let user continue using the app.

```
indicator.startAnimating()
let input = inputText.text

DispatchQueue.global(qos: .background).async { 
    let result = self.ballSorter.calculate(input: input!)
    DispatchQueue.main.async {
        self.updateResultText(text: result)
        self.indicator.stopAnimating()
    }
}
```